import numpy as np
import tensorflow as tf
from keras.models import Sequential
from keras.layers import LSTM, Dense, Activation
from keras.optimizers import RMSprop

# Caesar Cipher Function
def caesar_cipher(text, shift):
    encrypted_text = ""
    for char in text:
        if char.isalpha():  # Check if character is an alphabet
            offset = 65 if char.isupper() else 97
            encrypted_text += chr((ord(char) + shift - offset) % 26 + offset)
        else:
            encrypted_text += char
    return encrypted_text

def variable_shift_cipher(text):
    if not text:
        return text
    shift = ord(text[0].lower()) - ord('a') + 1  # Shift determined by the first character
    encrypted_text = ""
    for char in text:
        if char.isalpha():
            offset = 65 if char.isupper() else 97
            encrypted_text += chr((ord(char) + shift - offset) % 26 + offset)
        else:
            encrypted_text += char
    return encrypted_text

# Generate Dataset
def generate_dataset(size, seq_length, shift):
    texts = []
    ciphers = []
    alphabet = [chr(i) for i in range(97, 123)] + [chr(i) for i in range(65, 91)]  # Lowercase and uppercase
    for _ in range(size):
        text = ''.join(np.random.choice(alphabet, seq_length))  # Random letters
        cipher = variable_shift_cipher(text)
        texts.append(text)
        ciphers.append(cipher)
    return texts, ciphers

# Parameters
seq_length = 100
shift = 3
dataset_size = 10000  # Number of samples

# Generate data
texts, ciphers = generate_dataset(dataset_size, seq_length, shift)

# Character to index mapping
chars = sorted(list(set(''.join(texts) + ''.join(ciphers))))
char_indices = dict((c, i) for i, c in enumerate(chars))

# One-hot encode the data
X = np.zeros((len(texts), seq_length, len(chars)), dtype=bool)
y = np.zeros((len(texts), seq_length, len(chars)), dtype=bool)
for i, (text, cipher) in enumerate(zip(texts, ciphers)):
    for t, char in enumerate(text):
        X[i, t, char_indices[char]] = True
    for t, char in enumerate(cipher):
        y[i, t, char_indices[char]] = True

# Model definition
model = Sequential()
model.add(LSTM(128, input_shape=(seq_length, len(chars)), return_sequences=True))
model.add(Dense(len(chars)))
model.add(Activation('softmax'))

optimizer = RMSprop(learning_rate=0.1)
model.compile(loss='categorical_crossentropy', optimizer=optimizer)

# Training
model.fit(X, y, batch_size=128, epochs=20)

# Function to decode predictions
def decode_sequence(seq):
    return ''.join(chars[np.argmax(vec)] for vec in seq)

# Example usage
test_text = texts[0]
test_cipher = variable_shift_cipher(test_text)
x_pred = np.zeros((1, seq_length, len(chars)))
for t, char in enumerate(test_text):
    x_pred[0, t, char_indices[char]] = True

preds = model.predict(x_pred)
predicted_cipher = decode_sequence(preds[0])

print("Original:", test_text)
print("True Cipher:", test_cipher)
print("Predicted Cipher:", predicted_cipher)
