import numpy as np

class SimpleRNN:
    def __init__(self, input_size, hidden_size, output_size, learning_rate):
        # Inicijalizacija težina
        self.Wxh = np.random.randn(hidden_size, input_size) * 0.01
        self.Whh = np.random.randn(hidden_size, hidden_size) * 0.01
        self.Why = np.random.randn(output_size, hidden_size) * 0.01
        self.bh = np.zeros((hidden_size, 1))
        self.by = np.zeros((output_size, 1))
        self.learning_rate = learning_rate

    # Unaprijedni prolaz
    def forward(self, inputs, h_prev):
        self.h_prev = h_prev
        self.hs = {}
        self.hs[-1] = h_prev
        self.outputs = []

        for t in range(len(inputs)):
            self.hs[t] = np.tanh(np.dot(self.Wxh, inputs[t]) + np.dot(self.Whh, self.hs[t-1]) + self.bh)
            y = np.dot(self.Why, self.hs[t]) + self.by
            self.outputs.append(y)

        return self.outputs, self.hs[len(inputs) - 1]
    # Unazadni prolaz
    def backward(self, inputs, targets, h_prev, learning_rate=0.01):
        dWxh, dWhh, dWhy = np.zeros_like(self.Wxh), np.zeros_like(self.Whh), np.zeros_like(self.Why)
        dbh, dby = np.zeros_like(self.bh), np.zeros_like(self.by)
        dh_next = np.zeros_like(h_prev)
        # BPTT
        loss = 0
        for t in reversed(range(len(inputs))):
            dy = self.outputs[t] - targets[t]
            loss += 0.5 * np.sum(dy ** 2)
            dWhy += np.dot(dy, self.hs[t].T)
            dby += dy

            dh = np.dot(self.Why.T, dy) + dh_next
            dh_raw = (1 - self.hs[t] ** 2) * dh
            dWxh += np.dot(dh_raw, inputs[t].T)
            dWhh += np.dot(dh_raw, self.hs[t - 1].T)
            dbh += dh_raw
            dh_next = np.dot(self.Whh.T, dh_raw)

        # Gradient clipping
        for dparam in [dWxh, dWhh, dWhy, dbh, dby]:
            np.clip(dparam, -1, 1, out=dparam)

        # Ažuriranje težina i pristranosti
        self.Wxh -= learning_rate * dWxh
        self.Whh -= learning_rate * dWhh
        self.Why -= learning_rate * dWhy
        self.bh -= learning_rate * dbh
        self.by -= learning_rate * dby

        return loss

    def train(self, inputs, targets, h_prev):
        outputs, h_prev = self.forward(inputs, h_prev)
        loss = self.backward(inputs, targets, h_prev)
        return outputs, h_prev, loss

# Generiranje nasumičnog broja i polja s podacima za trening
def generate_custom_sequence_data(start=None):
    if start is None:
        start = np.random.randint(1, 100)

    inputs = np.array([
        [[start*10]],
        [[start ** 2]],
        [[start / 2]]
    ])

    targets = np.array([
        [[start]],
        [[start]],
        [[start]]
    ])

    return inputs, targets


rnn = SimpleRNN(input_size=1, hidden_size=50, output_size=1, learning_rate=0.001)
h_prev = np.zeros((50, 1))

# Trening
for epoch in range(1000000):
    inputs, targets = generate_custom_sequence_data()
    outputs, h_prev, loss = rnn.train(inputs, targets, h_prev)

    if epoch % 10000 == 0:
        print(f'Epoch {epoch} outputs:')
        for t, output in enumerate(outputs):
            print(f'Time step {t}: input {inputs[t].ravel()[0]:.4f}, predicted {output.ravel()[0]:.4f}, target {targets[t].ravel()[0]:.4f}')

        '''print(f'Wxh:\n{rnn.Wxh}')
        print(f'Whh:\n{rnn.Whh}')
        print(f'Why:\n{rnn.Why}')
        print(f'bh:\n{rnn.bh}')
        print(f'by:\n{rnn.by}')
        print(f'Loss: {loss}\n')'''

