import numpy as np 

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

sequence = [(0, 0), (0.2, 0.04),  (0.4, 0.16),  (0.6, 0.36),  (0.8, 0.64)]

targets = [0.2, 1, 0.5, 0.9, 0.4]

# Initial weights and bias
W_xh = 1.8
W_hh = -0.5
b_h = 0
h_t = 0

# Learning rate
learning_rate = 0.1

# Number of epochs
epochs = 10

# Training loop
for epoch in range(epochs):
    total_loss = 0
    
    for t in range(len(sequence)):
        # Forward Pass
        x_t, target = sequence[t][0], targets[t]
        h_t = sigmoid(W_xh * x_t + W_hh * h_t + b_h)
        
        # Calculate Loss
        loss = (target - h_t) ** 2
        total_loss += loss
        
        # Backward Pass (Partial derivatives with respect to weights and bias)
        d_loss_d_W_xh = -2 * (target - h_t) * h_t * (1 - h_t) * x_t
        d_loss_d_W_hh = -2 * (target - h_t) * h_t * (1 - h_t) * h_t
        d_loss_d_b_h = -2 * (target - h_t) * h_t * (1 - h_t)
        
        # Update Weights and Bias
        W_xh -= learning_rate * d_loss_d_W_xh
        W_hh -= learning_rate * d_loss_d_W_hh
        b_h -= learning_rate * d_loss_d_b_h
        print("x_t = " f'{x_t:.5f}', "h_t = " f'{h_t:.5f}', "b_h = "f'{b_h:.5f}', "W_hh = " f'{W_hh:.5f}', "W_hx = "f'{W_xh:.5f}')
    
    avg_loss = total_loss / len(sequence)
    print(f"Epoch {epoch + 1}, Loss: {avg_loss:.4f}")

trained_W_xh = W_xh
trained_W_hh = W_hh
trained_b_h = b_h

# Given sequence for prediction
sequence_to_predict = [(0.1, 0.01), (0.3, 0.09), (0.5, 0.25), (0.7, 0.49), (0.9, 0.81)]

# Initialize initial hidden state
h_t = 0

# Predictions
predictions = []

# Perform the forward pass for each time step in the sequence
for t in range(len(sequence_to_predict)):
    x_t = sequence_to_predict[t][0]
    h_t = sigmoid(trained_W_xh * x_t + trained_W_hh * h_t + trained_b_h)
    
    predictions.append(h_t)

# Print or use the predictions as needed
print(predictions)


