import re

def check_capital_after_fullstop(text):
    # Define a regular expression pattern to find full stops followed by a capital letter
    pattern = re.compile(r'\.\s+[A-Z]')
    matches = pattern.findall(text)
    return matches

def count_full_stops(text):
    # Define a regular expression pattern to find full stops
    pattern = re.compile(r'\.')
    matches = pattern.findall(text)
    return len(matches)

def count_capital_letters(text):
    pattern = re.compile(r'[A-Z]')
    matches = pattern.findall(text)
    return len(matches)

# Example usage
text = """
 me with your kindness? I may be ungrateful, I may be mean, only let me be, for God s sake, let me but at tat tan wount tou tan ton that bat thant an I tot yout the and  noubn an and fot thast tat thet tan  an ththat be the bet te and the s thel the stos be tot be the the thant tan ye s the ther the to t tout an tan t tostep fot s tot te that touln the the th tos be an te the s to a that pat an the te that the the the the therot at tas an that ton  an fot bet teubln tot be t the ton the to ay th
 me with your kindness? I may be ungrateful, I may be mean, only let me be, for God s sake, let me but an a in wop tared he, thath te wul yob tonf thef pye mouowe theatI he taban At the waln  Wansnotarot It tas hent con be thand but they I yopn an ban an ans herdes Ped and thit Hand  by try shalt bns tiot hes Eveuspt inst beg at tans an  av andnt rell abpl comen he bs y an wat fout 
 the pf s tern terteu ton topnatheoeace tt  I fo s tot young tett shy bt th I lain hy ton hat  Wa tt ann  Ma lis 

 the bench exhausted, and helpless, looking at no one, apparently oblivious of his surroundings and in t a d me the m m t t me me t m t me the  me m t t t t m t me t me t m mim t t we thin me me cte I m me the med to t se m me te the I ce t me me tim me cr f t t mom t t ce me t  t in t m m me t me me mo than te 
 t t met  I m se t me  t t t t t m t t t t me me be a the cre  Be the ced I  I d me the re t mor cad mim me the the t t le, t t me t t t m t t t t t t t f ge t ce ce it ant  t t mat  I 
 the bench exhausted, and helpless, looking at no one, apparently oblivious of his surroundings and t  soe e  S mind step tit  lesc re mor t bave To to it  ca cmand t ca thinkn t Atondre ped slat wu be cid. I n ino I  hime I  com c ieve be n to thet thol an me thet at  Hit I out e f t ce  t te  mint twcllit himt ne mort I) nhnirut 
 cod meedome thas  got t tey e t yor I m thed nored mica unk f y nit besd df c m t as of  Mr mices I t ce seche  ind me  c mlm cicm fe r,  re mstice,rep inor  t  I a 
ow I am troubling you). There is just one little notion I want to express, simply that I may not fore t at the at thed atrat fated at that teeas, at the tate thit t seditt the taye at at at t thet ate that at the that t te te the tit sathe that the thate the fas that d t tt utet t t that testit, the taditt d teat at nar tit I. I d thet sat the te, las tit and the outhe the sat tou, tt atrat tou byot tad the sit t that t te t at t ater t tat tor yout at ate tither thet at at sast tad t tid y, at 
ow I am troubling you). There is just one little notion I want to express, simply that I may not foriat I, I reryot ic? thated ttu Is, sitry t.. Tetit, suts e id was ace,, sat that at tyatd t,   ee It wf
. at at thatyowathatty, ye 
utoctid
yas,., coum ofat t tare gpat nay? ie. 
 oudd th es of th ticetsudoye but con atd t aty
oandhte ourto th d thay...d mteddyyoll ditt t.that ther ystrasalee bit Sisty, elag tdedlise nhe featdy twiters tit, Te watev st a I  it,latt,tyhe as s,, crceuste hawratyud v
no one had seen these men, no one had heard their words and their voices.
Raskolnikov was worried tast allly hinolllar har tor ereingd berert ol yoted torind bonill here arraid sid tad ard it tharr bout atd theateeencor arad rarg ang bery antor ing sard atod siat arad al ror buld burerrinor ar thimithisg sithid arit bustat therar abl gad berall therarlst aroid bu arsingablllllling bust aing told butr thim arred or all therist ingilf ald and bere hasalll if thery and bar and biterlllra hing.
 r
Raskolnikov was worried tir arlcor. Iresstiry tartrid al Pril bidgt... road Yonstall bl ar.. Sowhiklyin tood waklald etroif bud intcoc.. int augdiass and thef rod blddely lyad fadarogimthe borit, 
illvrrdy Iarnz audnd baz barstis oat?l.. erarv atrubgwistleaily in thit bacnanestigh il brdtro monresat,od sgomenn oechisold orland caid big aginyoily bucedthrev, brodd.. yowavloing idf, Loug.comed,, chirid An touf lin raz cous
o confirm the last hypothesis  a man commits two murders and forgets that the door is open! Finally, acee meemee cencee and cemce mecee ateee hed a he wace as cas, and and cemee che cencee cemee, andeee ceee cant, m and wame ine nase chemcew ceeeeenand a and cemce cee wacked was meat cere feringen meen mece, and mecingacee mame, humed andenee, ameeeeeeee and, hemee andee and cereeandee, acee, sacd mecer, cincem, and and and hace me wirceed ced mearecee, and and wanee hant the ande wacd of ceence
o confirm the last hypothesis  a man commits two murders and forgets that the door is open! Finally, Ras wiabm m cint, ctorndiyticencea a itee senev, ped coucee  cer yidmear..
 Ic leol at hem,ceeeem hice, Rantr anellm a hemceyilarewaneisc,. Tad, eeee tsed the cencee cad memerinf, fimowey iptteeetieaninceeicee I ateicee he wo, acereinysecee keryes af sdam?.. I matritcerey f. Bxrt ceryeand chead d rodcem,, onemceu nteazackefttom,de,chacerey of ceetoereavetly here tewof cacnwongyebl conico,che 
ep somewhere in the midst of candle ends and all sorts of rubbish, to pay him for the room and leavet t ther t t thely  arrt wingt tory wis t  ther ther  ther, thter ther the witert wout w tre t wer therathe t  tha wheven ther,  h whe whar  out t an ser whe t on ther t t ther ther  the t wa he ht wher, 
 the ly whe t t theve t wis whit witherst what wat w wheert tht wner a  here t t thit oute wite hithe t to hit yort t wht w t t the vatt, wat t ther thent s hor  
 wa se whir therrt wass y on 
ep somewhere in the midst of candle ends and all sorts of rubbish, to pay him for the room and leaveryt pacle tevluls sst,  tt  w? on w, the  lo voprouty, the d lit tort wr a of nc t f lritro w aoway vy, ans wats swet tresty, rntrthed thang woyowpy vrin wf w 
ingiinid t pumue wwirn whovovvatt say towis soon timarert  threr thateclry ttheece the wn d t douris an ft n c,  sray, sng irghetr yha beentwy an ly ist sneovrao Arethertn, Sh sit ing srsw trryiss y w ane wttu t waded
 heilisv thersewitwwt
at bubble s burst and gone for ever. But why are they such fools? I gave Zametov a bit of a thrashing he an the to  no 
  fe coo to  hoe fo  no  foke foo do  f ag. fo  ho   ou t an tol. a do  and hero  fo  fol he oo that an tho  to and a an  fo  I ha fod and a of a te The ave a and he whal oa nou t, asat an han. fo  bet I an and  fee fou and an a dou  an and  hat hee fo  fe an the the a do mou t aoueou to  no  fo  an ing of te  fee and a and fed and ha tat and tou at an ton wand tou heror tou d
at bubble s burst and gone for ever. But why are they such fools? I gave Zametov a bit of a thrashind c. po,. Pe hadiopiat no bec, a to wat ataeda woniofl, as e en yod, oooo houltiuera fo  noupea. 
 ns inag. And aaduef the fodeed peand  Ye tou t iovciont dod lor wesanadp..ive exve wfod torher oaped  to The pe oncona hougof euel,t  na oodd thascff shad an at fessericon I haetats and Huchard ara a fo  an to a ase ofuged fourofi do Hend ,he cef  a a tee ffe  fo  ord Ton inod of af an saoke  nofed T
me that Koch and Pestryakov were going upstairs at first, and is there no evidence about that? 
  N helly a canmul com was molyoulll was wally an san lin mowingimesano hingarnd wila lly oullis woung  lyoules hevenwally a mevangawingoull anyoull thas whellinet and thing wised bull therd an wis lu t so, lum, The souling wongananou whalllis molly thoman houly and min wallianond he molllon bulin ve llu thangontwelly en wat inoull la  theroulllllllully a mam alowing thong soung woum anoullin silly  
  N with tinilyid we hi... wilao, mullllinly hyyicoesty iowinoulior  the velwouy shat ancillug u shat amorn, weaf hyily wanlongevlo youved  thingavy lofsulllt surnulas youm ygo s f theut. Sowinances was myoomly. I in souks chad. I wanms divow ashisker hiningeslo wlis Ipuw hir winhumllt lnoulorexr s t mun lelll vemivag n woungelte 
 thad hingave,r, thoms mly nhithafpll covevun mom sens daveoilhevlort
.. translation,  muttered Raskolnikov from the stairs.
  Then what the devil do you want?  shouted ald tine se ou ionge le momiminge  oungand  te tong old fod an wofo ded tone  tolld at od s of for fond s ee fou fom fom inden  at somelf sone com seee tome mee foud told ome s of  old would tong of ind of ans of alll comed t  on off oung  forin de fe folne be foeden wome fouco dome de molle fon t t of t  I or e ton Pe e s cou fove an tondee me wo com  P folle seede she de foume dot an he sof  imc
  Then what the devil do you want?  shouted herite led  omen hofuleeougidiflit komo fen.o iod.
  ol, fis fouctof. mons. Sithe whe the cimoung sooucc,Pid t cir, sed tol r who st old f  re son fecemp ome fon  wom se  olfeewooft atteon w,edel she it oors ou mid ce fild rongldin s feicher ongldom wm ildmalnt f ofe ffold, mollf  y we,mid anes tol  cheantig fos tesnin olskon e cold fe st fees eeimnl rro er ro got mond om dondeond, t felites meld
ked such an action, and if necessary, I, too, will swear to it,  Raskolnikov said at last in a firm has of ho of hill a ho h thas of he of he he soo hat he he as the ond of ho hert the be hol tom hime hhe he he he hom wa alm, he has som hesh soas ang oft she he hhe hell hers she she he fom mo h ho of he the sho he a ge he he hisg ho h and the hhe ham half he ho shas the as he he  he  he he ho she sho mo helng he mo h he the hat he he  hom as thagen he fo he hhash fowhe  hoshom  he has  he thoss 
ked such an action, and if necessary, I, too, will swear to it,  Raskolnikov said at last in a firm sheed fept ofoflheng wa hibly corlou ad. wom ke of is mer ha hh wab wimnlfin ofy iftem pla hershrasemas he miw ho toot oess e. 
fh shashans e vro . wo hr sovov foll thig, I fen, hholloaleg bufs hat hee hhe tham shy e tol re bhesa g llilay Thehs shot thes that polllc
 themis of ant hilo soallllpls gor afhe shas, ag bou doae wor hat oplim  to sso gre wa ar ha oamove shad af O woek  ofins mull hion
me he woke up before daybreak lying on the ground under some bushes and could not at first understand tot fore serenten Rrer ster ther senann non and thert...
Rasander naterent stere sererserenden non tere spen Rasrenssens tet norennont per yopedring youpen ent sfound, hann thand tet pas tryen stont and sind pind andert ther in surp stey tere hand, tand therentonfenpn Rannas nor pinsern Rantent and Ras nar t ter sernrand toserendnen san tarny Rat sor..
Rantont ton there ser there sere tore ste
me he woke up before daybreak lying on the ground under some bushes and could not at first understanlnd, ther to anding Ras? Inat resnersst bner!t ssardvoserhendentsinnen narsen srepanre sferennppongryad teo!ssolnnrwanrund t thacRanserelg teyed s ey arleepod passtopt thaso suy syor s!rtrer!, pon the shernttesfensererenthern sanke tearI dere Iys snanered osroror cetrrit ot thorsontastanen. Rissedve fay..ve ad tan re setsresnhfon an, at rrrerten anR unsy ban uses theses thena s sor s..
 Rec amny
T
lly,   if you only knew what he was up to in a restaurant yesterday, though there was sense in it toug on s an an thou ice the myoure mug tis the souly beey bur ce you be blan un athand in mas mite same cur you sat an the ind be an  an the pus couger him sime foumyoum in the mrece bur saly can mexe myou she mancere mumyo the mom cer cas sor tha un your ancoug ang an ouf salle s be youme be the be youp at  anne bor s coug yan ly mus  here the sort y mer ce mas the a cre be bur camy yas sulme gon 
lly,   if you only knew what he was up to in a restaurant yesterday, though there was sense in it tomcirso lain. s am ofpoumyorring amyyrepblyerextor sous burincincoug bipneind olm yhtharytorm a ref mis pu ageycanlure.... tile yer  hearcarras.. An mangllan teqaf hupim,  tend con a  thin a cyos I th a mrhou she mastom  a cfovat cand eysamn bus yilsvime cat shyalce an sabdrat aoucer. I shonarove ! buin  orre cac Anas shis buumyofyine m, becobely walo the crems yer ce boc What in aut, co drrat has 
reer with, but instead of all those picturesque and monumental things, there had simply been some rily ene thee a we terer the the the ter thee terer an teas and we ine tee the the the tee.e the the thee the touly the that ane te ate te the an to andere te and cot I tate teet te the the the she  tey and heee se aly the we the yee thee ane teate teel  toullen in tee ther tout the me an the in the and we ine te and he the tey en te the an ee in thoutte yon the ye an te e he  ent at yere tee ther t
reer with, but instead of all those picturesque and monumental things, there had simply been some rigee yhtder, hee yitun bee verenury  hinge oucerielyde e tey hery, Heteenet bathay
 iled 
 ye ove weetendezy the inys, ta sebey thee blenlyy the Kove Raste revs? ye ten aber foteectter aite teen., nirn, b ires 
we Incavne  ten at mere nibe 
h teet thefe ou!t e they thet the sean aitery see tas nowrerte be.t, ex y., bee elee   thee nee  Thayory fae  te ree  hereaan iny,t ivett Ple onon thawtake
been up twice and found you asleep. I ve called on Zossimov twice  not at home, only fancy! But no meree that to woe hingeeedo the to wout the the ent wort anged he tere to hown he thee the here themntenthenongenthere herterete he stentered whet the tow he to he ther thry the thelntede theng there toug woth toule one the they wered the yo wange gout the theret he tat the the he the yor the yor the henee heseree the oneenoute weree the theng thert the the one theng wereter the on wrerreng wouthe 
been up twice and found you asleep. I ve called on Zossimov twice  not at home, only fancy! But no meaghot the hoer healkrt. She honttehe rothondeagnod hut hele. wrePer therantey areor to heemor! s, thert ete lotetere celot wot i heep exthoenee towntere theledongnoueonth hlitt, Pee hort yowe, mep ghy thrat anan ther yomedtwerleg Hhes the yong.....
 Bertedt woth moot, inel theent teyotroned Rhevedreeeaten ut woy ulee exhnon won he then thellyowy the and whow hi he averowhoey an hooro toly kers. 
 talking in husky voices. They were bare headed and wore cotton dresses and goatskin shoes. There wed And thee mashe hed hee he hatis he mit ind in the thee ther hi the hee hemere ha here and in them ding in thint an hed hith here he her sin thamre hee theee oreuthe the wist the in me here han rore in no he thed sin  hering hed se thee ind the  tout hit ge in mere wher hime ind the wede hither bere. An theme there hate me he sedeed it s mee itee at ther wome the sing heen has torere here and tit
 talking in husky voices. They were bare headed and wore cotton dresses and goatskin shoes. There weidg moridightibere! i houl in henveanind thiske syousint?.. I
 of tof ring ancomin, Po whise, owing mitit ate hed I
Aoby   I
 ond s, usteerthe meiditht. what, inmisdidtrurd tedrin iet Razhid  vome ted matike thet thesmeninen to ore gorinimne gath in it biflifg iot hime litt sel go le pelicken spit. Len helmihire, an math Oeeeeit dreireme  . the onee hin gimrrisf,
 I ithtee fu, hed hramitde ting 
ng on a chair by the wall. You can t fancy what a cancan that was! The girl was ashamed, blushed, at do de the hind wale she foulde  of she le  an win se  of hit  oulld we an  he  of shed fike te he he ind so se himl of woule fo the fo so so de fo e the fill she  our  he walll de lad in ped fo he fo so he we fe hed s fo lle  or he was he  or he we an fo hich an s of hil wathes fo she an an thed mo to the he le se s she whe he he in wise s of s le fe a he fo an the ther fe le se fech wis he lade 
ng on a chair by the wall. You can t fancy what a cancan that was! The girl was ashamed, blushed, ath 
oklin  wake ale  an Lak ie ffee hale kes was re dee mie beld ly ofkeey ane an iwe whe follallind s he dos alkelka he def Wanf folldsg  haw he sof Yous pe to ins. ge m a do blin lelene we tald 
 folse fild whe sy fedit.  I hie hiket tid ged te he hese Raderhlorent ho amene If whe atelh ind nlim shem fon he illl win hed  ber cfe hat the whye ne dfeln don s wyhe  fe deme thet 
 ro t, buderile ya
n oracle. He stayed just ten minutes and succeeded in completely convincing and comforting Pulcherian the almeuacl mace. atied, and at an und and areace careaced axdored, augererer and act ase and and at and and ind and anellattourellaill dond are alle on arain, ar and, aud and ant at at piteand in exa mor all alle ou alllace at and a mand at and cas al he pas diced and loming amea ard are aaninge arelled and alle allamer alleer a and, axlale ared are coud andereaded at asteraad, aid are foreall
n oracle. He stayed just ten minutes and succeeded in completely convincing and comforting Pulcheriaper ad at....
 Buin goured riceablaag deetced, ulp, soacad aup,imapeing  Yhe laadage,) he facnler act inud, a auraask coaddor, yhu g a wibsegres, . Agoligolll! H. I puedid ixcallealn,  Reake areaid datogis, ceih, din, ure.
 thell, aor ony astteard youd hacanalland walu, lolinig Houeliil weras apl, bu liered ioul, ulshulser to cro erelend  fathed, aacaaciter, can lucl, willddriculnin cull, Sirourg
lking beside him. His legs felt suddenly weak, a cold shiver ran down his spine, and his heart seemed whaked wathe Rete fill we pice, the faple in yow and thes coull wis wing  tat wit s the s s the prou ther ll whad the pe fillitelf wes wis f sto pe le thed and yor all fink in wlr wove wint  and the pent fas f stist  Whing  Whe whew the pet,   wat thit inon whak wore folll wint blan wike! He pive t pelln the pit and and pet he wast wowe wis pepelp the felling the what wore dof wat hit wict whell
lking beside him. His legs felt suddenly weak, a cold shiver ran down his spine, and his heart seemet,, fvit, falw. I  Whe on abl,! wayteiblre him,erit fll bet w thatif 
 tonle now stipd. nowt whad ot thempaling wied lowing haml, oppevly the sis nowra  somd spake  frrort them! wfowe if cack at ill pory  toew the lett wat nit, tlanienat fen attrougf aim it  to  Tat Rlunk agh met tiveres he werpsy at If whinit. sow stas st,
  ry thaseng  pas frankange,af tuill to whad faw len pllns inced thile sp
meet your suffering, then put it on. You will come to me, I ll put it on you, we will pray and go to dutto for re to  hat on tould that there that fre ser  rot sto deln ter re tounint tan thoure that therd and tit the the the ther the tot tall ar thed to that the the that te at the sen worn  I the cit there a dut thered thould ut to re the ther thouls ther that all of toll till co  the hertere tould yould to dut dot wruter exse waut lat that art, in the the ce the s the there, the therele there 
meet your suffering, then put it on. You will come to me, I ll put it on you, we will pray and go to fit wre thallan nover ul llit  at hu kear, lallon cer betoll oulntave, ald allis. I ow tieres ll ther loit aldrsout ttoln   tapl stol, blo done tootcult tell youdlt the wind hrleera, her the linze tiry gne, ne I fortrhele,, he thence fo  foretther  I ce  hed he shothe lottingI I kne for thorca lusvot stane, llal lare, latull, toll, an the muncsourt, tas tira nouserve ticol sulllyce the ald ar on,
of a major, and also a companion of an order, so that she really was almost the daughter of a colone she the the the the thing she the hey the the ser the youre the the the fre youf the the he hey wat he hey he the hem we the was the the fes hee hee the fouthe the the the the the the the the then ut the thot he her y of the wat the youth the the tho Pet he he the the that he we the they the the he the hey yoo he whey te he was the hey sher you he the thelythe the hey yought the the the the the t
of a major, and also a companion of an order, so that she really was almost the daughter of a colonerse to  hot That tertvo at anpsyt hems them the per sod thilly yhe e at pesth yothty yusthe whe wat wert huthe? Hly youy the thate shetc  tey tey he washsan wey s, hoch hery hey at thedht of ut I ove y theys to yit by opy e ser tot wis case?  the watht rew I ouct. Bly the yout thy seaky he padre ce thet the firl yoo fos they hashs chy be utt I sswupte ay theth het he sser hat ughe ald of the fed w
grateful to you, but I don t need it now. I can always earn my own living. Don t think me ungratefuln me thoung in younkon wou gonge pe whe ge wound he poughe pund bo he wing ho woun we wan be co wo puttee s werere bese he whe benen wan pou woubngine thene beean woun boungon bouthe whe bu we wo we woouten whe whe whe boren he unoing pe woou whenondine he waud wang ver be the won bout mery pu wer boungon whe pe tion wan wan he the whe preen we we win oun weng und the whe hnin in long woungon whe 
grateful to you, but I don t need it now. I can always earn my own living. Don t think me ungratefuly oud onf betlenc? 
st the kel yor hind the wun pup wigk, oninowhe thavno tougg sud lfa! butine inn tead ebe no s bunkop be founk nowd eve apap s oungnon they buteann pbe sougm, hane wenk on whev corpn h cickn, warferendbenon n youpabenon he wat obes poreromenos. Supgap woud bopep be whokovnoving weng ne thecp.itheg boke woou we pof uny oug  orele uf pous hening wang Iy hed ou. an ouru gesco w wu
gne, and for which men are crowned in the Capitol and are called afterwards benefactors of mankind.  he  has in f s p as ang son s an sould  has  hau he cu l  sou s  co d s he gos and  f and an  s  and anin  f lls s  co  f  p we cou s d  ld and as  soused s d se gov p gh   ald s ano  non ano  f sou ape s f s ane las and  I  f s ald s s s ll go  llld s s ld n  has co   hous ou hasasou d s sou lo d  f  housfe and l  ou s s f s p s d  he d  he d  ald f d ald d ch  he  and alf c  fof and  off s as n
gne, and for which men are crowned in the Capitol and are called afterwards benefactors of mankind. shougullofas  l  s sn  tocluon s ln gn, a sou f. W, idsaald afce t if c thtuckis lls r sos f r chuus pn y s al  ily c aco  d e dfoulln wep  Sf. S pofs sg lnr fh thln lf inger sdot dowadd ald,een m. Hf pnco. y apps sdof  pp we afe ablos boved y gocuon an l tersshed hus tus f cu Naw  ffu rod n  anlceno ld bf lf pale s t sou. By and s b  and ssg sus d suldhingoll pibl s ghn. s pou t ouce t ls se ce f
ress on every word and looking enigmatically but sincerely at her,   I should be happy now. You must  the fher tout to ther  f  at re the f abe the  f to the f at  it to he the f o ce the f to the f the f at t the foute to the the the wou the be the fo the co f tof tot tom the f   as f f re  hat at fo the f are wo the ve f he f o t the f he the cer fer wage wout ta f won the f tr the wot to to ther the wat the trethe f tore hef at the f on t to t at be t the f a w of of the f het w wor at  af f 
ress on every word and looking enigmatically but sincerely at her,   I should be happy now. You must rthe wh. ugowo  dice he wher frat i ch f t h forar 
 thot  ftot bay ff wom a ko peentc bof  at wag at owahe Brer ot a P re rhavagco feltr w  y apanibo wobe? t  cat wh ced as wenjeee q wd afab my.omeoffrer aet cad  Sert are no ty horyyas se w wherof tat wh bet f hit tWat at acd the on o wed do cing he afe 
 tos o rer af  s heagaatote let t cangeretorofy my Afre achowhey re the Hh s being owit th
 well, though,  Raskolnikov thought.
  Damn them? But the cross examination again, to morrow?  he ster the therackt the tound tought t prett the thering the the not the the the the kntt thent the the nore, the therete there the the there thand the there and, the cat tan the, than thand and there the the, tound and ter pronk the kn hat that tot the the toupt therent hut that the te kut tat the the that and to kent ce thet and the theret tenke theret the th and the the te t therect the the the th
  Damn them? But the cross examination again, to morrow?  he set  het the.no nork, Rant the, I st oket cuskot citt tovy Ne thoutounghtac thiel, teutht ancot and dee at kur ind ouch Naskprce..
 Ran! ntancey ju h the gat tut y, werthat therett cheo at n thasspeinkand thene?, at thtattelit arasthe toutt thand thy kaspe mudkesta poupe the nound tard  n, meng yot thee the, toped ce tance fett onted.t peckowe tot lopd, toutut tit, the ingc ap thaekad. I  b, ce lop
 damn them! Are they asleep or what? 
And again, enraged, he tugged with all his might a dozen timeghe luse hhathe whest sesturgangage wasteale ghat highe fout the wasedishing waseding hit lugheughing and whing wist mfing woughifushishiugedgh whend lf whenging wasilughe wetheed we ghat ghat highen he he dhat s fou the gh the hasenheaghaset souge suttenghe theuthandening wing whand shinhe the dealed sust wasthe ghe sut she whe ingowed wandingh hang ard the phingaghout theughanging wasening sheng
And again, enraged, he tugged with all his might a dozen time yorrcfhagis tetwat 
 aghastowighandhet tstiughatwd hla whabistgutr mintape naweasttherud yowe shesuved fut ghe ahts l wumllost abd hagheng ubsanony
ers dutgandeyve cou ghe wot ged, sthhe thaveg whendeinucik tut, g tesaksengiy..t he! ingeng, reldighe n tdhoughund llot the ghtnoug thet sitoughthhwtt heltolfau hetlog yhomhal.. the matlatuthauls xist ng hiotyelthtthangay bidist ghhhounnesghongall s
vdotya Romanovna, especially at that moment when she was walking to and fro with folded arms, pensive wat mat tere dealle be tandat wallen taspeing andoweet wealle andonand ande wa wandere thou hoonand ande withey 
 andongert pist te ande wouthate inedent lot on wo the waod and the the one touthe tan was waln we me tame anderome wat ore se bere con wa wireret lanelereeen was wat autongonit to ma andongon wa and the anallerind out to  thet and the the tit withe sme ware ando ghe touele fowere in
vdotya Romanovna, especially at that moment when she was walking to and fro with folded arms, pensives wistatet the lao abled wotavne Ie,euth fawenint .lert wateerilep ilanget anx anem anailat pet whe whange toranke. Tideytroeared wated, 
omlas I Aneeepgingio la  ollne splewerel abin aed aowe oal neise leilnsobell fange to ge en pow thagst anavaiere they yrorell. fo me kee. andatolle Heatlineve auena shous leouaere itanearel  hantiliild cele  tomeus wgided ovareap wnaselous waandr ser ithire tt
r,  he muttered in confusion, not looking at her, but pressing her hand.   We shall have time to spe withe now the not wot the s nonithen therstening thetle senten the sthenithor and thorlot hed ther ond hand shanntringenitning the the the ther, thentongow the then the the he wotdond theonnstingt ond ther ther thatherenteny the knee the chen the thinithestthand the titeand hather! then yoneinod,hereentttong ther therthen then yonting stolle the thed tonen the he thing not hed thing not yotthente
r,  he muttered in confusion, not looking at her, but pressing her hand.   We shall have time to spengeannst lo yhed hnitheyserrelyenlyy nostoiy thit to thts? sotthy hhey fot thotsrnry Rande,rr th  onting nifthe govn te fy drt hedere then teouwing wobyowitte sornottctang nootteur!.. therittow ind rl intenis nt y Swit dln Iet, niltaur  hedol pkfisthand and!d ndor tr.
sereatd thatcondeprelly thers,t gow thoth IPoningoreastere ton bl ineesathes th, as th that. ounte clmedes ngerst thin? hasnd, te 
d to shabbiness would have been ashamed to be seen in the street in such rags. In that quarter of ther and and tathe win, ave , at andelnean wongin and sorer, anow, noullr, ion in wan  and soren,anin ang, wounou and inoutelanout ing sareronealinieln and.... Herean and, oo, bes oonerane, selinano rinone, wan  fan, honet angorerer............. yoonellnad and...
  role tonerer, seredooneacer, nou, n anond Tou and has..
orererourenoyeleras, and and...
 whanialnow anden and allllle, san anneleroan
d to shabbiness would have been ashamed to be seen in the street in such rags. In that quarter of thingornenernon outhel...
ed andoap, tat. alwa,eny,ron, wallsrorou. , 
Sorse and.Plodatonel,waz ther. slmingalfste...cl folinosnal, wed.onantinleloninomes... Howy thatind,,o Ron,y, soomhe ha las sren anoo rle yoeod....., ang thateastooren.enolo 
 Soun,eninold.elrislllo,...
 oed,rlanor. soenleridorioa I Wfse neesairoret vers,.
ot,and,avo  Sonablis ween annerinow soramk,.Souterd,outole wimaplwow 
sy braggart of last night? Was it possible to imagine so absurd and cynical a juxtaposition? Razumihis is toof it is oodsst tis I iis sous initofod I sousine ininoofouinit sised s sisis is of iitom tous sied sit wos sim sinis sionous so wouspous ous iousooniin sis sist in as isit tor oo fos se sous mus in tos in ititonis in s sout tousinis in inis sous sorisp is ispous ioo of seiss iodispas I suod iseos sisinos se  I inor ook in outousid is iser oonis sit poonousit tout aonos sou I sous in ous I
sy braggart of last night? Was it possible to imagine so absurd and cynical a juxtaposition? Razumihi toff so  oo of setoltot piis  ofodotos toulonie nttw aoreaicomasom nes siomp pikivigeon th abocuisejicy I s So iteofsoriteomk fup,inomscirnibjapdisein
ousis oodrisllenon sop.  I in futhe!sins lo med t iss. so  oongst befutothome ofin ind!soskev sigeanoy ss ifisn vis netintaiopeotee an ube t on I I
o  Ye osiu im sp dilft osiss sroddoltto sosss ciniodiut inisp aofisr f sointon fotis ipst yiodean
as a crime. I ve never, never been stronger and more convinced than now. 
The colour had rushed inthe  hane I and and then tner then tene br and and ne  We and anghe and and and and and alt sad and at ar and the sad and the han and y and and and and athe  he ban at an an  aa wavy mamed and at and andy the  he andy t thathan  aen and reay ant measiand y vean y and an y ed he dre hed and and merea wand thate ande yo theme ha hed and arate y end and anhere y mer, cas ay ant ne an in andere and yo 
The colour had rushed inthase av hark dar yo yaremy by antian toft mom wataedeamekanhfat tlyat harghe h andemyeran thewade. sed more tanoutctepy ns P  hem wathay thnethehean ave pimyeanazy Sd ant Redraed anmere wee th,we an de wan eaded tat!ry ve san oattre yoantel ante yok ave hadd ba h e wlayds heaseinthey and anrymange had ay  fn thed, y y sarhedite it at ken tond wine ta g ande st yhey rid thore thewryery but, hateon 
 feeling as suspicious as ever. And at eight o clock this morning  that was the third day, you undera  hos all hin a hen h man hin  h a  hat ll  he  he  ha 
 han a ha llinge on  ha 
 ha ha ha th  
 ha  an  aning hin  hit  he ha  An  h a soll. bom  
 he  h  ha boul sh ha  as s has hing he h  
 an shing the he  on  ho ha d  he  at lle a sa hall halll sha 
 ha he ais a ra lull had h  ha ha lo h man a ch  ha an  h aol  has ha d h hin  h s  hing a be all  
  a  and hin as a n  ha he  h  he ha h
 feeling as suspicious as ever. And at eight o clock this morning  that was the third day, you under hou he an as t pl of  I  Y sous  outoist aly ult sa it  hl  om  Hh las hnd havisg t hsbea hy  od  Heaf  hon  ct wourl aa  o ware tat on hal cing ont  he. hastha ff  f I hesus aly hod he   hawd pe st a Pas ale the hanisc y him thing hille filshory iarat 
 an tt azl qun 
 aa 
 cuok  
n ch 
 hupa 
 ghe hougn  ancr  ad he bad no hazl sh meof sing hol. tas h hish tim he pre h osad rretl hea  Als 
s not so at all! That was all Katerina Ivanovna s invention, for she did not understand! And I neveras therit of thed the ala the theris tout ane the waiino anders aite allatang te aneede wat anane anathe pit se toominga sane wata wenat at and west ont Inga wat wime souneatinga anat anat ant as sit tinga and ton I. I s nat
 satingat aed taing ata ha wingain anongat tate ting we wougd ind tal se and angais Ianonga sast and there angat Ie an anetere to wingarnestouge tow sateat t anof I ta wa wan
s not so at all! That was all Katerina Ivanovna s invention, for she did not understand! And I neveryedootives anhetg wivet at. 
materemikbendakn towanonk. Iug and prs...
a wan hit!. Iingaay... lk wing souna whewat wis she seratg. An mnik se
ibneskd yoinderssonifklming tindaiske. Aer ingvenowtl teraiazat I ond wifcteorauneellaikndis con s b. Is lalmetott t tat
hedoinedpae anend. wegingowetavitent adeustowe se waltoth,t angribccting se, not setrrt antonin shecct therein kosstl heakok sasonkiket
e. And I m ever so fond of reading all military histories. I ve certainly missed my proper career. I anon has and an an an an an an an an an an an an an and an n an wand an an a are san an an ha had an an an ave an an an an an an an an an an an an an an an an the an an an an ad an  an an an pa an a f ten at an an ave an ara and ann an an an tand an a and an and do the an n in an an an a then an and wand  non an at an an an an an an and an an an than an an an an a and  hen and anoa nin a t an an 
e. And I m ever so fond of reading all military histories. I ve certainly missed my proper career. I dap and am ant nT aned an anisat?.. natdr in an! ais an mranisson  at, iY d at ha fata and ino thes aned marut ban It. carede talf an gaf, noran a s ato lnie  ond wny and w h lid buncac a faingacarmea n s mat a ha knd anounrh ourg, a I ra hand ge t ha band  hine an  at at a the hn mn yniar afin the we in h ar a Ro td frtn h s thareacding cnar ad I tofes mupt nf ad  hat wot ane in at ca dis prn an
le night feverish and delirious. She jumped up from time to time, wept and wrung her hands, then san he herel in ine hin th sherel shere she I ser I sin Rill she le sh le pe phe pee I d I shin inin re I I she pen the in  his  he himll was chel s e she she  he I pee hisher in the him she she he  I an her an she I al the  he he.. I he I ill in he pee he pere I I pithe ereringin ine I I she  he I ap she I win he his he  his ely I se hes I she her the inin ip in she shis he hil in shish he in sh I p
le night feverish and delirious. She jumped up from time to time, wept and wrung her hands, then san
  hishinm hereepmor live shile It Re I she I cou  whele An. Re I the drwha I i loly in fpr likirel heerorin yhe f exn s ry I kr? ther s, nt I ptp pen th bonsinot Bule hetper. I ineea, sis hin il cher amhed 
 mithin he  ole in I nhereuth hereepse hin m. shime te lorks he I, ghe ie edl the Rushl s.. I Oowid rly wirer Ihel! Thillikptm h fh ye I I sur lf eppesthy whe re h Iisletrhen e So Anu heinhepp
  And Porfiry. 
  What does that matter? 
e tell it to? You and me? 
  And, by the way, have you herer his ind at hid hareiseas andhear thathingingingousshishatherrathah hhe sushe hithouthear hingishirat sasd sas nis sherharishare hing aas and thera dheshashin the thand hashathat hiss are here am rereri se arere hishississ as shaindingast hirhed and hare harhindinistingarh hingereristhes here harheraingingastithit thad sis she ais ind ag shere as asskard as ther haram has shed anghingingis sh
  And, by the way, have you He oe hhseide uth, at ir and hareengavt chastin, ac,t himseane! her har hac sorhiudimeiyithimoiterared atherereoverhara hisherst ssth at hasaisehe chen toourouromaseerhighingirfousishet howh hire aing as neeranthsreriahr whiufrery, het ss any hy th, ir talisd as shtthaindisho ab, alf sondir ind gishin, fry h horhachithyihoreRa frh the th sofras Arerarhoutethinger has that iutena tajlas.ungi tiat s
 he had other things to think of. The jewel case is a conclusive proof that he did stand there.... Toes ses outhe owetsett tersened atene stt the there st of the toettere tere toot the the t ing the toutte stoethime tin  It siowenge stoowe the soune the the tinot the to tore stouttete stoostoront the tore the tok tie the the. He soet sed thestouthe tore the s oute sering the stestouth tore the the tere tostesttoitto tond weestof the tithe tous stere seastoote s toott stoe ted ted sed wee setyets
 he had other things to think of. The jewel case is a conclusive proof that he did stand there.... Thend tenaket y dnd set. I  wioe cice  d terseet, ters. ctet reeast medo  otowa h. Tits theroeny the titreff th atkers aas terand senth shem, weryeshe it thitenet  tecindougiy iopettoet ene sgs He sgstoute thisupltted srespere ied heset th s theyet tttistord owee yef seeisid e iutath the odenthastoeget pathersfreceeeritweon itiritet as i  otenthat ceit. Hetho h sieve pe s e itTe anh tnet os of a te
ng to become the principal of a boarding school.
  A boarding school, ha ha ha! A castle in the airhi till aly the ll dithe fa dit  hid it arithat s ar thicill llld toulllllistoull gare ther all hulit the hed ll am llingaid ald thally alllly and ull Pare ml cisier iald and and alllaikid a thand til tit to gas and ther then t daly tham bll to chr ha if cinge saind as lid the hallat ind sid hallit ifo Pur arlll all sul t ind and t car t tid at ha laid alict caull coug an ar and wis arit thould ar
  A boarding school, ha ha ha! A castle in the aire tond t ollldime hr Pvit thellit  pousAd fidd wirangead me fy of habe, naid ell qry by ondt aal  aedysiaaldtly Pyaldne lli haiftimerouldt  igch, I av ind ofalid, ciat oriifly Irl tob am lingl tricire, bll hiance waind iriascictiad and, bl bnaat rr ina Nicred t cey hald oulatll hevlocl sarteo Iitriwialr afdy axf Pab aricyid thiwat hamich wit cav P cly cudid wasiall me fabdt  rory brl iond urd w y 
 than his own, Razumihin saw at once that his visitor was ill.
  Why, you are seriously ill, do yousdey ahe y ee me dus houn y eddenereely...
..
 d y a walle eeene hee andye eandece mee andeneeeene hehe theeeeeeeede heereeaskehereae yen  ecerme nd fe ed neduly anded hande he he wand heaneeadete ande ee fe me and whe seee hasereee whereee ee hed an he fhe ye the youst cees Pe.e ande hehed yound.. He yed y eond. Hed  d and wh he heee and a Wse cume cedeeehe hee weeeene h s feeeeee se the he he
  Why, you are seriously ill, do younde afehinneheeehagre.s...ne meeeere! ite, scesurfusere sousd cound afewaulicocand fa Theus h andybe..
a. elmeely and thefeerey yan. dsy handyuw Bewidede fe s.
.....
dlaneehe dee yout, hee al houdd ehaveyin why d she the wware neesaedeeeeees dy w, d. Hayut leefeebreehekeingeee shecyanded te duerhor  Bugnpeheeehear e ean? desnu.  ac ndeeaye beeaye ine Aned sed eneihuceeveeme....... dme werly.. 
 back. You look and evidently see nothing before nor beside you. At last you begin moving your lips wi  to  h fir  iou    a io  t  ur w  at  u t athi it  t t ain t it t win  io  Aid it a ha ict aind  a P  ad hi t f ir w ti  a tt id  w riing  a hu a ait t io  ou if iid e  ad hit a  aiwit ii r  iat po iu t s cu di d it at a  d aing a  u d t it a   wit ii in  hio  i. e to  g thir a hi f ain  t thit i a to d ihe t ai iid adin t aitfiiio  io   r ti thidit io  i ind in  ht iou ii t ti t at in thin  hi
 back. You look and evidently see nothing before nor beside you. At last you begin moving your lips su  in  u d a let.i situ ce Ynd thillireaisin t e ae sedidvap aiwfiat i, .u ehio did  f , fle!  tr Air t ahen t atialaidd t t indiod i scof yiou t lifk t aiou . ad p iud  a R itowimd fius Rltiitiian tas  Riciou a mit. Oe to t t uroa ou ouf hasiit rad o soo t drd ml a
 y t e huld gaddii  hie s la a rptd
 r t  t Rit  adr chmect aid tf t oce I. nzu a a i tras upe ast n  d t ia d  add mif w!  Wfidof 
 his overcoat and his hat and went out, carrying the candle, into the passage to look for the ragged s wat an llll sarr tereare me  asd  bll bd abll bell besthe  mer all  bla all fl and all masnd bere a lle all s his me she bed bld  ss anl ad bere sle her aras losthed all sased there hadrd walle were pl al be an bedrs,  be  ad dete athablly bomeras melleall bat asd atora her abe alllyousn asl there  a me sea be bd all bed ashelled and I bt ay ll bl bare ssalleher heas all all  aull abl te he hea
 his overcoat and his hat and went out, carrying the candle, into the passage to look for the ragged,omooker bll clehanhavdl sald. wibly. war waved samanabeh, Lo bndd lferelllyt. bl thorreh . Y! mald il war s  herachillyed them b carlar nwoule har pimdromum!sre he onrrathhehe wakee reyherrhiwe t wac ahow waskevee the bmd, assmema dofll ang a avedpedainsthee. we, fll lored. Weeesn move ok... He feard nd bed a awar wakftonabblf dho essmer ar, ssYel sathe cache. her hodd rha llld and  hiutsmfinimss
 don t say she gave it to you?  cried one of the new comers  he shouted the words and went off into ca me the ste te a as ie t e ha and tee caree ce tit se fee therd s de te she kere thered wate he teed s te he kee Ne s fe thereceme ce ta san is theriee ce cie me cee Deie amit ie fherof thed the the cee thethee suateed te ce fe teed there the cesthe ce tece the kethed se cere ceethere the te ther f yered the the the the ted ase the thed mee tok bendee died ateaite he t amesed and temicheridese t
 don t say she gave it to you?  cried one of the new comers  he shouted the words and went off into dr camee se hahetad cesthanieesket, andok the ceeece f thty eded t srea in tesracaaksheaskitrede therdesers Iin tithand eelt ande at t
udede tedok fo treckethede terem ste a medi heyikfontyecomeesharenodeheds 
aiTreyce.t ta somhakenifmife td iundes imr wy miyehet iis a ditot. Hit, bitfshiteate  fe d. pask kite here su tisedieLf fems cs  cedon reyterere caeye ty smid amenicedtisee,et pemyted aifeim
esire not to hinder your discussion of the secret proposals of Arkady Ivanovitch Svidrigaïlov, whiche was I at an all th a  as ape y raik an ad a ca ape a  as me a I  as he g  as anist an an tou  ang as ang ta an th a  and ay  as a  au  a wasas anc a lo a as Ia as ang th al ad a  as I anis as ly fa  at a s yo  ang all th an al  ape  an a  aas and al  ang th s pa ang  a me a sin  in th tis fa a as a  aanl  at yo you a  ape in a da she  aik ex a a an as as ald  I ta  ale io  be as  ala in  as and 
esire not to hinder your discussion of the secret proposals of Arkady Ivanovitch Svidrigaïlov, whiche frat ne aa sand ar th fa  ar mabaice yiy as a! ilg is Pa rol ing an ta aa  pom wiiaeive f  maly? av Waig them a  askinay, w  Plis ely ale r grel re rie! ca yo  pis  I at  byasdt t h I f w at vn t  oupe I le, fo ta bad tinily te his lyaa ae a w sa out tea as me ang w ala a yo biy as mu  anden t T a An diik maike Poa as an?a aly Paau  miy wate sbisto  aasilg il re to ait, in, s Iaperes acaly yo n 
o, but he did not himself quite know what.
  I may be bringing you something else in a day or two, And bo  Ao no be bo fo  A d  An  An on oo  bo co  And t on wo  of on a wo  And ly fin t  Andou bled  Ald   And.  I  Heln, br w  Aow sove  I  An  I wo tow bo fo  And  An bo wing  Ao no in wiss  oo  Yo be  And in win fe wo  Ao  Aod  An wo  How wou fo  Ao an bo  An s bo bo bn in w lin wo  And  Aid  Aol  An lomnlo bf  An  An ll  Andou do he bn bo bl  Yo bit  An w oo no  And on wo  bo  And  And  And  A
  I may be bringing you something else in a day or two, Aedeand bu fo ly ciu pow fort  hlkeanco  In woulolniy dre d mog. Hf hor  Afo c th ban, pe nof Hod fls fi  wo ikw ong. I And y, Ansve l, Hel  Aon e ff bo  A d  He meln se.. Hng nos nho alde ko t ti ma Zre snew wou ion, os I in ex one  so rs he bas, in wou. 
 woiln boulledabablom,  ba ifind Ad loln wpo...  hf wou. th on wood w  Aod dombll w  abn, co  Hed no  Dow, He bo wodhoy aplor  Andor bn. Ad  Al
mething particular.
  What surprises me,  he began, after a short pause, handing the letter to his s ore com unok on gomko cow mokk tomero  om to herov mo momer hero ho tome ouk fore she mom s oongov fous fongoncok he or, ot ofom fou witho the mom ou do momere toro comench she makeo and om h orkon youk ob ake she co toming ho com, mor, rome  youk onto  he co koo hos om ow ov somerowo somke whe  or momer  ho kouk omkof come s comong com, orock orek  co kong onching houk, orgo gof bom mo kous out
  What surprises me,  he began, after a short pause, handing the letter to his koctg os fors s Ikoume bofemictom it ho mowesrous momoyo  Shacome Io pro horolromkus s che vomk h Tomel ano shy cheples crch r Phowasseo wo s aik. vo, cor  ronovge ch tes theros.
o rea bogegdoute fongho g yo.  ont bore roms to umk suto mroug omare, h,ro gound ore wome sok bre shiconge Dheogm woug I oo gomes ofeonto oo mo my.of ucch mem vvedatsk ask momot, Iokk omshuuck tom, upoingom, attatiokk cou
o on, and, like many people, he exaggerated and distorted the significance of those words to an absumat timong angat, are, anat, anour ande, It our that markerkereroungerengathererourcerrerkergatk, herkromr, turarom, Herrerens sumrag arat, thark, sram, that morkgeremy bongas, atangare mask, mur merre, nomrerangrr, mere thange s metk, cem, brat, ier, it, aaker, sto thang areang, bat, torfabs anounth s mer, burmang then, oumer momy ther mes, athatk ther, 
rt, 
 te cerrat waskenger, areer, asta 
o on, and, like many people, he exaggerated and distorted the significance of those words to an absuseekruml, 
s the w ourrrirutsak sikmamer Nad mr thrramnt,grt aer, surmimk,t tomer Ingits,  Rusrewe mretrry, srems gak, ataskar O,ttanrru, merrrumpum mo hars jevs asreis, thare thrcroncbcofsharerthe gay grelas lomemy, ingatas. berowar,., carrate., tig, k ris, ther,therkandeikrhth ry.tema..
Tasik,s, per.
, hangumrutkrommagmr, m, fabontpedeverithi,, our at,ania troug Rask, st, thask, anirick, thab
ihin even if it were not close by... let him win his bet! Let us give him some satisfaction, too no dinged temere io morerme herd blmenerenten thet thent  Ierereenee morered the bere toun te len the toend thont me bred bevenen, Ioe me thentellee mereeteng erene thernd thenderee the teenertethelle the thendeneretind mo and nout mentente le llll too oorlererenternt ed ened bred te  I tonenderidemenee moremen sume hellleroutit ed cithere toupe toore thengenderonee then torere poreninden emernoure t
ihin even if it were not close by... let him win his bet! Let us give him some satisfaction, too no cere noolesdendetidde,ipentinoreaebeanorendeelny pcdedienmemef. toilanerd! boelmourt eomimrerermrn Ry wimnooomcy mongonentet lttredont tutied, iofid neone moout totiboitinan dpiedmrvey?anonded Id ildirt cerynshirnoexe gundelechefkceeserent the woerg mes berothiron  m. Helen one tbedhereneddenarl n eneepp r,en thtielul t toredrecimlp ey fore bern htemedt t ceredled boererndippe onhedepen le tal tai
ke of it yourself. 
  A journey? Oh, yes. I did speak of a journey. Well, that s a wide subject....n w tit tin inging in w t inoin  ing in whe w w wing therin in n in niin win t wine  in non in win in w  in ne itin t an wiin n thin nin in pe n in wn in the nin l net in wall in win wer the t t in n w to  win t wing in winn re win therpinin thaingin w wien   Y t ine in an won no in win aner an nin in wini  ore in t thin no   W pe wing than wor n the win win bo in in non win wit tin  winer in n in
  A journey? Oh, yes. I did speak of a journey. Well, that s a wide subject.... excit wr,  fun y ingininrearer  n yo rin at routandorre inre wan nlen an ano wiin witer in intikslle wor , td t Br snt inpprt wino Her wo pH tit po nrie pe onttrin en yw  w was w se ts ble thie panin ili ind i nkihertinity purt ote t tren t sn l tice hnn crnofr iren wwe s  ditu  hallonownas, wir ce wan pet tulonnne hais on net ilin ing inmpinck r n m thitrpownin wi, er wry iero trin  wmin hiner,i
f a sudden I saw a little girl of thirteen, nicely dressed, dancing with a specialist in that line, led he sor he med he med bem 
 athen and her mer herh hed hed he mer a she mes hind de end and she hed she her her me heme sher the hem he ad there ah the har ere mer hat bes ham herme de hempemer he er ther she  Whe and her her de her al he the and sout len as hem hed at her and ill  himin haed nhe sher ing she men her de ther bemer er he she  Ro sher mee mee heme he dee and her the where mer su
f a sudden I saw a little girl of thirteen, nicely dressed, dancing with a specialist in that line, merd bes saint hit heas in he dar hect sile thed heeaangh blerde her!r ereld s med, a siby rt mon mled eeofll sthe weee fe theseme muer  Hen hard bre b vemexerle mer assl here endiene the se hrer  ampep the hamd bimanllrt soud hat 
 a snir ko he anea meedklyher armt drris ald sathhrly s sho mir le wiee  mom reeth atg. in ong shentscp find we  at thars le pevtessid med he dee sempe orgeedrsnrecedy
. Do you think him clever? No, he is a fool, a fool. And is he a match for you? Good heavens! Do youn ce s te met in in it be be ii we i y we ie ik mimin y  be the the tit  a the alle ke the maly lle he rel he ti al an y a Ie hit ay be the moml  y s le s me g he be me mou iid me thie tine I halin  fit be te w y Ialliche me me be cave il ie an imo anti. aie the me a the iime  in ie he se then the sir he lly ie y He the y lat ie ce,  a ale live y He y ie ce alle le bimol ie i  he mit ou hit il ay 
. Do you think him clever? No, he is a fool, a fool. And is he a match for you? Good heavens! Do youn, hend wow wowl Boit w it. Riilite bl tee ay bem Il  iiie lly  yote a hawi seake aly iiso sdiinind ile ti  O bm bedhiimale yavary inge y  lallyamencarangie ag. ity Insthe wit hile  Rie himrm f me pi , Let ono aly bly hoy lno  he moemiov pamnalexti th idatit Pino m, e alee we hi, ne iily Su thead ar yimime thele deming ii hat I b blli bofti ledit asica wey temlie niml se gry sangive y Hid was  aip
g such a Schiller? I bet you re imagining that I am trying to get round you by flattery. Well, perhalland and Iand sanand inan and Irstarof and Int yond in thandingarit in far faskid Iasen.. and in tiathand liin arsin. I I aner inallyiarenan. Inland and inaing...... foraton thet and sanalingaskillint arll............itin sas allan aiss as fainging and sof far thandlland Ifans athikngand Iatismlengalliin fars wallianinint allend Ias ant..ant in tharingazllingand...... Hingisingy at sist on. Iathi
g such a Schiller? I bet you re imagining that I am trying to get round you by flattery. Well, perhatonan sanpytcrik paings ahinyinankinst.  her Iathin.faihllap Ilitaikan.. Igian salendslllarlaify,intiiskiplyind ananere the ladn.. Iarf, rsrpisiknclliasghas aialinri..
w. blleiland y
w lfad, fitaweaznt eofondin.....ouellifinnern fiblengainnilasiliate Iiiurall,hdnfeandhita angwdI....ilivstelessaakit tin blinthn wissingl...exay in ikulg th a.heriir...tImisk ffan, Iitoweaillist exat inakk hislanipr
 more intense, and if he had happened to meet Mr. Luzhin at the moment, he might have murdered him.
 mee hangharenwave mevenseereas ger an wa wa men ha a ne and a Her waeghe bes 
he annwan he henee and heree wasee whe wat ang wale be waveerenvens aste wateeree anise me was herenm wo heree whe he waners thee was ae w wat ehe he he ay was onsheree se. sen eas ng ve we wae ann mesw wing wereer he ange as he annes be womeing tne an. he bemee ane ingen. he a her shas haveneanee re. aeas navne then 
veeennHe. mentanes.... at Rhe
He he ngry me.sieo be we aineame.
 I vniwereat? vev,e Rving g.. heeerwah!
gse ghlyet oes a bau whanin!? lyo mmaiithimen neesrvve lasegea, ha ohe I wnghe nhat olees Ieve wer, m anden, s,he hi xo.
ee eonmee, banlne. ss mull. Be amer ymppo shea. ovhemese bin, loneangivngeh.. iighengc worrenwaem he
nlmsvewity 
 Reearaisevsss as haga, shuuming wave.. redes vnv han
"""

capitals_after_fullstop = check_capital_after_fullstop(text)
num_full_stops = count_full_stops(text)
num_capital_letters = count_capital_letters(text)
lines = text.splitlines()
capital_count = sum(1 for line in lines if line and line[0].isupper())

print("Number of lines with the first letter capitalized:", capital_count)
print(f"Number of capital letters after full stops: {len(capitals_after_fullstop)}")
print(f"Number of full stops: {num_full_stops}")
print(f"Number of capital letters: {num_capital_letters}")
