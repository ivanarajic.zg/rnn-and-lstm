from __future__ import absolute_import, division, print_function, unicode_literals

import numpy as np
import tensorflow as tf
import io
import sys

# Set standard output encoding to utf-8
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation
from tensorflow.keras.layers import SimpleRNN

from tensorflow.keras.optimizers import RMSprop

from tensorflow.keras.callbacks import LambdaCallback
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.callbacks import ReduceLROnPlateau
import random
import time

start = time.time()

with open('texts/Candp_original.txt', 'r', encoding='utf-8') as file:
    text = file.read()

vocabulary = sorted(list(set(text)))

char_to_indices = {c: i for i, c in enumerate(vocabulary)}
indices_to_char = {i: c for i, c in enumerate(vocabulary)}

max_length = 100
steps = 5
sentences = []
next_chars = []
for i in range(0, len(text) - max_length, steps):
    sentences.append(text[i: i + max_length])
    next_chars.append(text[i + max_length])

X = np.zeros((len(sentences), max_length, len(vocabulary)), dtype=bool)
y = np.zeros((len(sentences), len(vocabulary)), dtype=bool)
for i, sentence in enumerate(sentences):
    for t, char in enumerate(sentence):
        X[i, t, char_to_indices[char]] = 1
    y[i, char_to_indices[next_chars[i]]] = 1

model = Sequential()
model.add(SimpleRNN(128, input_shape=(max_length, len(vocabulary))))
model.add(Dense(len(vocabulary)))
model.add(Activation('softmax'))
optimizer = RMSprop(learning_rate=0.01)
model.compile(loss='categorical_crossentropy', optimizer=optimizer)
model.summary()

def sample_index(preds, temperature=1.0):
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(probas)

def on_epoch_end(epoch, logs):
    sys.stdout.write(f'----- Generating text after Epoch: {epoch}\n')
 
    start_index = random.randint(0, len(text) - max_length - 1)
    for diversity in [0.5, 1.0]:
        print(f'----- diversity: {diversity}\n')

        generated = ''
        sentence = text[start_index: start_index + max_length]
        generated += sentence
        sys.stdout.write(f'----- Generating with seed: "{sentence}"\n')
        sys.stdout.write(generated)
        sys.stdout.flush()

        for i in range(400):
            x_pred = np.zeros((1, max_length, len(vocabulary)))
            for t, char in enumerate(sentence):
                x_pred[0, t, char_to_indices[char]] = 1.

            preds = model.predict(x_pred, verbose=0)[0]
            next_index = sample_index(preds, diversity)
            next_char = indices_to_char[next_index]

            generated += next_char
            sentence = sentence[1:] + next_char

            sys.stdout.write(next_char)
            sys.stdout.flush()
        print("\n")

print_callback = LambdaCallback(on_epoch_end=on_epoch_end)

filepath = "model.keras"
checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')

reduce_alpha = ReduceLROnPlateau(monitor='loss', factor=0.2, patience=1, min_lr=0.001)
callbacks = [print_callback, checkpoint, reduce_alpha]

model.fit(X, y, batch_size=128, epochs=500, callbacks=callbacks)

def generate_text(length, diversity):
    start_index = random.randint(0, len(text) - max_length - 1)
    generated = ''
    sentence = text[start_index: start_index + max_length]
    for i in range(length):
        x_pred = np.zeros((1, max_length, len(vocabulary)))
        for t, char in enumerate(sentence):
            x_pred[0, t, char_to_indices[char]] = 1.

        preds = model.predict(x_pred, verbose=0)[0]
        next_index = sample_index(preds, diversity)
        next_char = indices_to_char[next_index]

        generated += next_char
        sentence = sentence[1:] + next_char
    return generated

print("Generate text after 500 epochs: ", generate_text(500, 0.2))

end = time.time()
elapsed_time_hours = (end - start) / 3600
print("The time of execution of the above program is:", elapsed_time_hours, "hours")
model.summary()
