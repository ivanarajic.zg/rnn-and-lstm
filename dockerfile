FROM python:3.10
WORKDIR /rnn-lstm
COPY . /rnn-lstm
RUN apt-get update && apt-get install -y \
    python3-pip \
    python3-venv \
    git
RUN pip3 install --no-cache-dir -r requirements.txt
ENV PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION=python
CMD ["tail", "-f", "/dev/null"]

