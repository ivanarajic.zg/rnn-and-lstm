import nltk
import re

nltk.download('words')

from nltk.corpus import words

valid_words = set(words.words())


file_path = 'gavran_LSTMresults128layers.txt'  
with open(file_path, 'r', encoding='utf-8') as file:
    text = file.read()

pattern = r'----- Generating with seed: "(.*?)"\n(.*?)-----'
matches = re.findall(pattern, text, re.DOTALL)

def count_words(text):
    tokens = text.split()  
    valid_word_count = 0
    invalid_word_count = 0
    for token in tokens:
        if token.isalpha() and len(token) > 1 and token.lower() in valid_words:
            valid_word_count += 1
        else:
            invalid_word_count += 1
    return valid_word_count, invalid_word_count

for seed, generated_text in matches:
    valid_count, invalid_count = count_words(generated_text)
    percentage = (valid_count / (valid_count + invalid_count)) * 100
    print(f'{percentage: .2f}')