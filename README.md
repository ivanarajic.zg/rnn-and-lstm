# RNN and LSTM

*1. Prepare virtual enviroment and install required packages*
```bash
python -m venv myenv
myenv\Scripts\activate (for Windows)
source myenv/bin/activate (for Linux)
pip install -r requirements.txt
```
*2. Run program*
```bash
python program.py > output.txt
```

*3. Docker*
```bash
docker build -t rnn-lstm-image .

docker run -d --name rnn-lstm-container rnn-lstm-image

Enter container to run program
docker exec -it <container_name> bash 
(docker ps to get container name)
```